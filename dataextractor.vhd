library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;   

entity DataExtractor is
port ( 	clk		: 	in std_logic; 
	    add 	:   in std_logic;
	    modify	: 	in std_logic;
	    delete	:	in std_logic;
		data	: 	in std_logic_vector(31 downto 0);
		q   	: 	out std_logic_vector(63 downto 0);
		en		:	out std_logic
);
end DataExtractor;